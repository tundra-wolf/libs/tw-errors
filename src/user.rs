use std::path::PathBuf;

use thiserror::Error;

/// User errors, resulting for faulty, missing or incomplete user input.
#[derive(Debug, Error)]
pub enum UserError {
    #[error("invalid command '{0}' provided")]
    InvalidCommand(String),

    #[error("invalid script file '{0}' provided, error text '{1}'")]
    InvalidScriptFile(PathBuf, String),
}

#[cfg(test)]
mod tests {
    use super::*;

    // User input errors
    #[test]
    fn user_errors() {
        let e = UserError::InvalidCommand(String::from("invalid"));
        assert_eq!(e.to_string(), "invalid command 'invalid' provided");

        let e = UserError::InvalidScriptFile(
            PathBuf::from("./test.rn"),
            String::from("file is not accessible"),
        );
        assert_eq!(
            e.to_string(),
            "invalid script file './test.rn' provided, error text 'file is not \
             accessible'"
        );
    }
}
