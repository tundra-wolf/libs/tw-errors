use thiserror::Error;

/// Generate errors
#[derive(Debug, Error)]
pub enum SourceError {
    #[error("no ASCII data in source text, data is of type '{0}'")]
    NoASCIIDataInText(String),

    #[error("no binary data in source text, data is of type '{0}'")]
    NoBinaryDataInText(String),

    #[error("unimplemented functionality '{0}'")]
    NotImplemented(String),

    #[error("no UTF-8 data in source text, data is of type '{0}'")]
    NoUTF8DataInText(String),

    #[error("no UTF-16 data in source text, data is of type '{0}'")]
    NoUTF16DataInText(String),

    #[error("no UTF-32 data in source text, data is of type '{0}'")]
    NoUTF32DataInText(String),

    #[error("source action failed for project '{0}'")]
    SourceActionFailed(String),
}

#[cfg(test)]
mod tests {
    use super::*;

    // Source errors
    #[test]
    fn source_errors() {
        let e = SourceError::NoASCIIDataInText(String::from("UTF-16"));
        assert_eq!(
            e.to_string(),
            "no ASCII data in source text, data is of type 'UTF-16'"
        );

        let e = SourceError::NoBinaryDataInText(String::from("UTF-8"));
        assert_eq!(
            e.to_string(),
            "no binary data in source text, data is of type 'UTF-8'"
        );

        let e = SourceError::NotImplemented(String::from("new function"));
        assert_eq!(e.to_string(), "unimplemented functionality 'new function'");

        let e = SourceError::NoUTF8DataInText(String::from("UTF-16"));
        assert_eq!(
            e.to_string(),
            "no UTF-8 data in source text, data is of type 'UTF-16'"
        );

        let e = SourceError::NoUTF16DataInText(String::from("Binary"));
        assert_eq!(
            e.to_string(),
            "no UTF-16 data in source text, data is of type 'Binary'"
        );

        let e = SourceError::NoUTF32DataInText(String::from("ASCII"));
        assert_eq!(
            e.to_string(),
            "no UTF-32 data in source text, data is of type 'ASCII'"
        );

        let e = SourceError::SourceActionFailed(String::from("read file 'test.txt'"));
        assert_eq!(
            e.to_string(),
            "source action failed for project 'read file 'test.txt''"
        );
    }
}
