use thiserror::Error;

/// Generate errors
#[derive(Debug, Error)]
pub enum GenerateError {
    #[error("generate failed for project '{0}'")]
    GenerateFailed(String),

    #[error("unimplemented functionality '{0}'")]
    NotImplemented(String),
}

#[cfg(test)]
mod tests {
    use super::*;

    // Generation errors
    #[test]
    fn generation_errors() {
        let e = GenerateError::GenerateFailed(String::from("webload"));
        assert_eq!(e.to_string(), "generate failed for project 'webload'");

        let e = GenerateError::NotImplemented(String::from("new function"));
        assert_eq!(e.to_string(), "unimplemented functionality 'new function'");
    }
}
