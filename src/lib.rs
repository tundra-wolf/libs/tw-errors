pub mod analyzer;
pub mod generator;
pub mod refactorer;
pub mod source;
pub mod user;

use thiserror::Error;

pub use crate::analyzer::AnalysisError;
pub use crate::generator::GenerateError;
pub use crate::refactorer::RefactorError;
pub use crate::source::SourceError;
pub use crate::user::UserError;

/// Tundra Wolf error
#[derive(Debug, Error)]
pub enum TWError {
    #[error("an error occured during code analysis")]
    AnalyzerError(#[from] AnalysisError),

    #[error("an error occured during code generation")]
    GeneratorError(#[from] GenerateError),

    #[error("an internal error occured")]
    InternalError(#[from] InternalError),

    #[error("an error occured during refactoring")]
    RefactorerError(#[from] RefactorError),

    #[error("an error occured while handling a source")]
    SourceError(#[from] SourceError),

    #[error("a user input error occured")]
    UserError(#[from] UserError),
}

/// Internal errors, caused by resource/coding limits.
#[derive(Debug, Error)]
pub enum InternalError {
    #[error("no source in error")]
    NoSourceInError,

    #[error("unimplemented functionality '{0}'")]
    NotImplemented(String),

    #[error("system out of memory")]
    OutOfMemory,
}

#[cfg(test)]
mod tests {
    use std::error::Error;

    use super::analyzer::AnalysisError;
    use super::generator::GenerateError;
    use super::refactorer::RefactorError;
    use super::source::SourceError;
    use super::user::UserError;
    use super::*;

    // Internal errors
    #[test]
    fn internal_errors() {
        let e = InternalError::NoSourceInError;
        assert_eq!(e.to_string(), "no source in error");

        let e = InternalError::NotImplemented(String::from("new function"));
        assert_eq!(e.to_string(), "unimplemented functionality 'new function'");

        let e = InternalError::OutOfMemory;
        assert_eq!(e.to_string(), "system out of memory");
    }

    // TWError object
    #[test]
    fn tw_analyzer_errors() {
        let from = AnalysisError::NotImplemented(String::from("new function"));
        let e = TWError::AnalyzerError(from);

        assert_eq!(e.to_string(), "an error occured during code analysis");
        match e.source() {
            Some(e) => {
                assert_eq!(e.to_string(), "unimplemented functionality 'new function'")
            }
            None => panic!("'source' not set"),
        }
    }

    #[test]
    fn tw_generator_errors() {
        let from = GenerateError::NotImplemented(String::from("new function"));
        let e = TWError::GeneratorError(from);

        assert_eq!(e.to_string(), "an error occured during code generation");
        match e.source() {
            Some(e) => {
                assert_eq!(e.to_string(), "unimplemented functionality 'new function'")
            }
            None => panic!("'source' not set"),
        }
    }

    #[test]
    fn tw_refactorer_errors() {
        let from = RefactorError::NotImplemented(String::from("new function"));
        let e = TWError::RefactorerError(from);

        assert_eq!(e.to_string(), "an error occured during refactoring");
        match e.source() {
            Some(e) => {
                assert_eq!(e.to_string(), "unimplemented functionality 'new function'")
            }
            None => panic!("'source' not set"),
        }
    }

    #[test]
    fn tw_source_errors() {
        let from = SourceError::NotImplemented(String::from("new function"));
        let e = TWError::SourceError(from);

        assert_eq!(e.to_string(), "an error occured while handling a source");
        match e.source() {
            Some(e) => {
                assert_eq!(e.to_string(), "unimplemented functionality 'new function'")
            }
            None => panic!("'source' not set"),
        }
    }

    #[test]
    fn tw_user_errors() {
        let from = UserError::InvalidCommand(String::from("command"));
        let e = TWError::UserError(from);

        assert_eq!(e.to_string(), "a user input error occured");
        match e.source() {
            Some(e) => assert_eq!(e.to_string(), "invalid command 'command' provided"),
            None => panic!("'source' not set"),
        }
    }
}
