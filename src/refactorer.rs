use thiserror::Error;

/// Refactoring errors
#[derive(Debug, Error)]
pub enum RefactorError {
    #[error("unimplemented functionality '{0}'")]
    NotImplemented(String),

    #[error("refactor function '{0}' failed for project '{1}'")]
    RefactorFailed(String, String),
}

#[cfg(test)]
mod tests {
    use super::*;

    // Refactoring errors
    #[test]
    fn refactoring_errors() {
        let e = RefactorError::NotImplemented(String::from("new function"));
        assert_eq!(e.to_string(), "unimplemented functionality 'new function'");

        let e = RefactorError::RefactorFailed(
            String::from("rename"),
            String::from("webload"),
        );
        assert_eq!(
            e.to_string(),
            "refactor function 'rename' failed for project 'webload'"
        );
    }
}
