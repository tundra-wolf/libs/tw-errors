use std::path::PathBuf;

use thiserror::Error;

/// Analysis errors
#[derive(Debug, Error)]
pub enum AnalysisError {
    #[error("file '{0}' is not a Makefile")]
    NotAMakefile(PathBuf),

    #[error("file '{0}' is not a shell script")]
    NotAShellScript(PathBuf),

    #[error("unimplemented functionality '{0}'")]
    NotImplemented(String),

    #[error("parse failed for source '{0}'")]
    ParseFailed(String),
}

#[cfg(test)]
mod tests {
    use super::*;

    // Analysis errors
    #[test]
    fn analysis_errors() {
        let e = AnalysisError::NotAMakefile(PathBuf::from("test.abc"));
        assert_eq!(e.to_string(), "file 'test.abc' is not a Makefile");

        let e = AnalysisError::NotAShellScript(PathBuf::from("test.abc"));
        assert_eq!(e.to_string(), "file 'test.abc' is not a shell script");

        let e = AnalysisError::NotImplemented(String::from("new function"));
        assert_eq!(e.to_string(), "unimplemented functionality 'new function'");

        let e = AnalysisError::ParseFailed(String::from("user-manual.adoc"));
        assert_eq!(e.to_string(), "parse failed for source 'user-manual.adoc'");
    }
}
